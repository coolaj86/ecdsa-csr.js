#!/bin/bash

# creating privkey
openssl ecparam -genkey -name prime256v1 -noout -out ./privkey-ec-p256.pem


# canonical example
rm csr.pem
node bin/ecdsacsr.js ./privkey-ec-p256.pem example.com,www.example.com > csr.pem
openssl req -text -noout -verify -in csr.pem 2>&1 | grep 'verify OK' && echo 'pass' || echo 'FAIL'


# 100 domains (max allowed by Let's Encrypt)
rm csr.pem
node bin/ecdsacsr.js ./privkey-ec-p256.pem example.com,www.example.com,api.example.com,assets.example.com,ftp.example.com,example.org,www.example.org,api.example.org,assets.example.org,ftp.example.org,example.co,www.example.co,api.example.co,assets.example.co,ftp.example.co,example.net,www.example.net,api.example.net,assets.example.net,ftp.example.net,whatever.com,www.whatever.com,api.whatever.com,assets.whatever.com,ftp.whatever.com,whatever.org,www.whatever.org,api.whatever.org,assets.whatever.org,ftp.whatever.org,whatever.net,www.whatever.net,api.whatever.net,assets.whatever.net,ftp.whatever.net,whatever.co,www.whatever.co,api.whatever.co,assets.whatever.co,ftp.whatever.co,sample.com,www.sample.com,api.sample.com,assets.sample.com,ftp.sample.com,sample.org,www.sample.org,api.sample.org,assets.sample.org,ftp.sample.org,sample.net,www.sample.net,api.sample.net,assets.sample.net,ftp.sample.net,sample.co,www.sample.co,api.sample.co,assets.sample.co,ftp.sample.co,foobar.com,www.foobar.com,api.foobar.com,assets.foobar.com,ftp.foobar.com,foobar.org,www.foobar.org,api.foobar.org,assets.foobar.org,ftp.foobar.org,foobar.net,www.foobar.net,api.foobar.net,assets.foobar.net,ftp.foobar.net,foobar.co,www.foobar.co,api.foobar.co,assets.foobar.co,ftp.foobar.co,quux.com,www.quux.com,api.quux.com,assets.quux.com,ftp.quux.com,quux.org,www.quux.org,api.quux.org,assets.quux.org,ftp.quux.org,quux.net,www.quux.net,api.quux.net,assets.quux.net,ftp.quux.net,quux.co,www.quux.co,api.quux.co,assets.quux.co,ftp.quux.co >csr.pem
openssl req -text -noout -verify -in csr.pem 2>&1 | grep 'verify OK' && echo 'pass' || echo 'FAIL'


# single domain
rm csr.pem
node bin/ecdsacsr.js ./privkey-ec-p256.pem example.com > csr.pem
openssl req -text -noout -verify -in csr.pem 2>&1 | grep 'verify OK' && echo 'pass' || echo 'FAIL'

# utf8 domain
rm csr.pem
node bin/ecdsacsr.js ./privkey-ec-p256.pem 例.中国,example.com > csr.pem
openssl req -text -noout -verify -in csr.pem 2>&1 | grep 'verify OK' && echo 'pass' || echo 'FAIL'
#openssl req -text -noout -verify -in csr.pem

