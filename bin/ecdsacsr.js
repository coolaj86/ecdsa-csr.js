#!/usr/bin/env node
'use strict';

var fs = require('fs');
var ecdsacsr = require('../index.js');

var keyname = process.argv[2];
var domains = process.argv[3].split(/,/);

var keypem = fs.readFileSync(keyname, 'ascii');

ecdsacsr({ key: keypem, domains: domains }).then(function (csr) {
  // Using error so that we can redirect stdout to file
  //console.error("CN=" + domains[0]);
  //console.error("subjectAltName=" + domains.join(','));
  console.log(csr);
});
